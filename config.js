var config = {};

config.comparedValue = 'First'
config.saveToDb = true;
config.sendToTelegram = true;
config.logToConsole = true;

config.default = {}
config.default.currencyPair = 'USD-EUR'
config.default.currenciesPairs = {'USD-EUR':{}}
config.default.interval = 5000
config.default.oscillationPercent = 0.01

config.mysql_docker = {
  host: 'mariadb',
  port: '3306',
  user: 'root',
  password: 'root',
  database: 'uphold',
  dateStrings: true
};
config.mysql_localhost = {
  host: 'localhost',
  port: '3336',
  user: 'root',
  password: 'root',
  database: 'uphold',
  dateStrings: true
};

config.telegram = {}
config.telegram.chatId = -1001444180118

config.mysql = config.mysql_docker;

module.exports = config;