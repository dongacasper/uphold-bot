const bot = require('./bot');
process.env.NODE_ENV = 'test';
// afterAll((done) => {
//   // bot.exit();
//   done();
// });

test('getCurrenciesPairs ', (done) => {
  let argvs = { _: [], c: [ 'BTC-USD', 'BTC-EUR' ], p: [ 4, 7 ] };
  let expected = { 'BTC-USD': { interval: undefined, oscillationPercent: 4 }, 'BTC-EUR': { interval: undefined, oscillationPercent: 7 } }
  expect(bot.getCurrenciesPairs(argvs)).toStrictEqual(expected);
  done();
});