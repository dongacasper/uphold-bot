DCD = docker-compose -f docker-compose.dev.yml

docker-build:
	$(DCD) build --no-cache --pull

docker-up-force:
	$(DCD) up -d --force-recreate --remove-orphans

docker-down-clean:
	$(DCD) down -v

database-init:
	$(DCD) run --rm wait-for-it mariadb:3306 -t 60
	$(DCD) exec -T mariadb mysql -uroot -proot < sql/main.sql

init-dev: docker-up-force database-init
