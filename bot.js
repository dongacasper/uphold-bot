const OscillationMonitor = require('./oscillationMonitor')
const config = require('./config')

// setting currenciesPairs object and start monitoring each pair
async function start (currenciesPairs = config.default.currenciesPairs) {
  let oscillationMonitor = new OscillationMonitor()
  Object.keys(currenciesPairs).forEach(key => {
    oscillationMonitor.startMonitoring(key, currenciesPairs[key].interval, currenciesPairs[key].oscillationPercent)
  })
}

//getting the parameters and run start method with them
function init () {
  var argv = require('minimist')(process.argv.slice(2))
  start(getCurrenciesPairs(argv))
}

// creating currenciesPairs object from params
function getCurrenciesPairs (argv) {
  if (Object.keys(argv).length < 2)
    return
  let currenciesPairs = {}
  if (Array.isArray(argv.c)) {
    argv.c.forEach(function (item, index) {
      currenciesPairs[item] = {}
      currenciesPairs[item].interval = Array.isArray(argv.i) ? argv.i[index] : argv.i
      currenciesPairs[item].oscillationPercent = Array.isArray(argv.p) ? argv.p[index] : argv.p
    })
    return currenciesPairs
  }
  currenciesPairs[argv.c] = {}
  currenciesPairs[argv.c].interval = argv.i
  currenciesPairs[argv.c].oscillationPercent = argv.p
  return currenciesPairs
}

if (process.env.NODE_ENV !== 'test')//for testing reasons
  init()

//for testing reasons
exports.getCurrenciesPairs = getCurrenciesPairs