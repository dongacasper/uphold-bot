require('dotenv').config({ path: __dirname + '/uphold.env' })
const config = require('./config')
const https = require('https')

class retrieveUpholdApi {
  constructor () {
    this.URL = process.env.URL
  }

  // To get currency pair price, the default is from config.js
  async get (currencyPair = config.default.currencyPair) {
    return new Promise((resolve, reject) => {
      https.get(this.URL + currencyPair, (resp) => {
        let data = ''
        // a data chunk has been received.
        resp.on('data', (chunk) => {
          data += chunk
        })
        // complete response has been received.
        resp.on('end', () => {
          resolve(JSON.parse(data))
        })
      }).on('error', (err) => {
        reject(err.message)
      })
    })
  }
}

module.exports = retrieveUpholdApi