require('dotenv').config({ path: __dirname + '/telegram.env' })
const config = require('./config')
const mysql = require('mysql2/promise')
const TelegramBot = require('node-telegram-bot-api')
const token = '1986964794:AAG--Fd-NAi9g0Hd4B5pWajcBwTdMETqRE4'
const chatId = config.telegram.chatId

const RetrieveUpholdApi = require('./retrieveUpholdApi')

class oscillationMonitor {
  constructor () {
    this.bot = new TelegramBot(token, { polling: false })
    this.bot.stopPolling()
    this.firstValue = {}
    this.previousValue = {}
    this.retrieveUpholdApi = new RetrieveUpholdApi()
    this.database
    this.interval
  }

  // start monitoring one pair and start the interval
  async startMonitoring (currencyPair, interval = config.default.interval, oscillationPercent = config.default.oscillationPercent) {
    try {
      this.interval = interval
      this.database = config.saveToDb && await mysql.createConnection(config.mysql)
      if (config.comparedValue === 'Previous') {
        this.previousValue[currencyPair] = await this.retrieveUpholdApi.get(currencyPair)
        setInterval(() => this.runPreviousValue(currencyPair, oscillationPercent), interval)
      }
      else {
        this.firstValue[currencyPair] = await this.retrieveUpholdApi.get(currencyPair)
        setInterval(() => this.run(currencyPair, oscillationPercent), interval)
      }
    } catch (e) {
      console.log(e)
    }
  }

  // run the monitoring by getting the value and compare it to the first value
  async run (currencyPair, oscillationPercent) {
    try {
      let value = await this.retrieveUpholdApi.get(currencyPair)
      if (this.oscillationCheck(parseFloat(value.ask), parseFloat(this.firstValue[currencyPair].ask), parseFloat(oscillationPercent))) {
        await this.notify(currencyPair, 'ask', value.ask, this.firstValue[currencyPair].ask, oscillationPercent, this.interval)
      }
      if (this.oscillationCheck(parseFloat(value.bid), parseFloat(this.firstValue[currencyPair].bid), parseFloat(oscillationPercent))) {
        await this.notify(currencyPair, 'bid', value.bid, this.firstValue[currencyPair].bid, oscillationPercent, this.interval)
      }
    } catch (e) {
      console.log(e)
    }
  }

  // notify the user
  async notify (currencyPair, priceType, value, comparedValue, oscillationPercent, interval) {
    try {
      config.logToConsole && console.log(currencyPair + ': oscillation, current ' + priceType + ' value is ' + value + ' compared to first value: ' + comparedValue)
      config.saveToDb && await this.database.query(`INSERT INTO oscillations (currencyPair, priceType, value,
                                                                              comparedValue, oscillationPercent, rate)
                                                    VALUES ('${currencyPair}', priceType, ${value},
                                                            ${comparedValue},
                                                            ${oscillationPercent},
                                                            ${interval})`)
      config.sendToTelegram && await this.bot.sendMessage(chatId, currencyPair + ': oscillation, current ' + priceType + ' value is ' + value + ' compared to first value: ' + comparedValue)
    } catch (e) {
      console.log(e.message)
    }
  }

  // run the monitoring by getting the value and compare it to the previous value
  async runPreviousValue (currencyPair, oscillationPercent) {
    try {
      let value = await this.retrieveUpholdApi.get(currencyPair)
      if (this.oscillationCheck(parseFloat(value.ask), parseFloat(this.previousValue[currencyPair].ask), parseFloat(oscillationPercent))) {
        await this.notify(currencyPair, 'ask', value.ask, this.previousValue[currencyPair].ask, oscillationPercent, this.interval)
      }
      if (this.oscillationCheck(parseFloat(value.bid), parseFloat(this.previousValue[currencyPair].bid), parseFloat(oscillationPercent))) {
        await this.notify(currencyPair, 'bid', value.bid, this.previousValue[currencyPair].bid, oscillationPercent, this.interval)
      }
      this.previousValue[currencyPair] = value
    } catch (e) {
      console.log(e)
    }
  }

  // check the oscillation
  oscillationCheck (value, comparedValue, percent) {
    return (value > (comparedValue + comparedValue * percent / 100)) ||
      (value < (comparedValue - comparedValue * percent / 100));
  }
}

module.exports = oscillationMonitor