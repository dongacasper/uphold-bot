# UPHOLD BOT
## Local
### Setup
Execute:
`npm install`
#### DB
If saving to DB wanted, it should be set in config.js using _config.mysql_localhost_<br>
`config.mysql_localhost = {
host: 'localhost',
port: '3306',
user: 'root',
password: 'root',
database: 'uphold',
dateStrings: true
};`<br>
then config.mysql = config.mysql_localhost;
then it should be activated: _config.saveToDb = true_
#### Telegram notification
If sending to telegram wanted, it should be activated: _config.sendToTelegram = true;_ <br>
The default telegram channel is https://t.me/UpholdOsc <br>
It could be changed by changing the chat ID from _config.telegram.chatId_
### Run from local
The app can be executed from terminal<br>
check [RUN](#run)

## Docker
### Setup
Execute:
`make docker-build`
`make init-dev`
#### DB
If saving to DB wanted, it should be set in config.js using _config.mysql_docker_<br>
`config.mysql_docker = {
host: 'mariadb',
port: '3306',
user: 'root',
password: 'root',
database: 'uphold',
dateStrings: true
};`<br>
then config.mysql = config.mysql_docker;
then it should be activated: _config.saveToDb = true_
#### Telegram notification
If sending to telegram wanted, it should be activated: _config.sendToTelegram = true;_ <br>
The default telegram channel is https://t.me/UpholdOsc <br>
It could be changed by changing the chat ID from _config.telegram.chatId_
### Run from docker
The app can be executed from terminal<br>
`docker exec -it <container_id_or_name> RUNCOMMAND`
check [RUNCOMMAND](#run) <br>
example:<BR>
`docker exec -it app-uphold-dev node bot -c BTC-USD -p 0.001 -i 5000 -c BTC-EUR`

## RUN
1- Simple execution: `node bot`: will run [default values](#default-values) from config file
2- Using parameters:
    I- each currency pair should have `-c`
    I- each interval should have `-i`
    I- each oscillation percent pair should have `-p`<br>
###Example:
Monitor one currency pair BTC-USD with oscillation percent 0.01 and interval 6000
    `node bot -c BTC-USD -p 0.01 -i 6000`<br>
Monitor two currency pair BTC-USD with oscillation percent 0.01 and interval 6000
and EUR-USD with oscillation percent 0.02 and interval 4000
`node bot -c BTC-USD -p 0.01 -i 6000 -c '-USD' -p 0.02 -i 4000 `<br>
if multiple currency pairs will be introduced with one interval or oscillation percent,
all will have the same value,<br>
Monitor three currency pair BTC-USD, EUR-USD, EUR-BTC with oscillation percent 0.01 and interval 6000
`node bot -c BTC-USD -c EUR-USD -c EUR-BT -p 0.01 -i 6000 `<br>
If interval or oscillation percent will not be set, it will use the [default](#default-values),
Monitor Two currency pair BTC-USD, EUR-USD with oscillation percent 0.01 and interval 5000
`node bot -c BTC-USD -c EUR-USD`<br>
If interval or oscillation percent are less than currencies, it will use the [default](#default-values) for latest pairs,
Monitor two currency pair BTC-USD with oscillation percent 0.01 and interval 6000
and EUR-USD with oscillation percent 0.02 and interval 5000
`node bot -c BTC-USD -p 0.01 -i 6000 -c '-USD' -p 0.02 `<br>

## Default values
It can be changed from config.default<br>
The initial set is:
`config.default = {}
config.default.currenciesPairs = {'USD-EUR':{}}
config.default.interval = 5000
config.default.oscillationPercent = 0.01`<br>
The default is to compare with the first retrived value, if we want to compare with the previous value
`config.comparedValue = 'Previous'`

##TEST
to run test:
`npm run test`