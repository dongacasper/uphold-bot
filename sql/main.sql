create
    or
    replace DATABASE `uphold`
    CHARACTER
        SET utf8
    COLLATE utf8_general_ci;

USE `uphold`;

create TABLE `oscillations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currencyPair` varchar(100) NOT NULL,
  `priceType` enum('ask','bid') DEFAULT NULL,
  `value` varchar(100) NOT NULL,
  `comparedValue` varchar(100) NOT NULL,
  `oscillationPercent` varchar(100) NOT NULL,
  `rate` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON update current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8mb4;