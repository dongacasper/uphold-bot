const oscillationMonitor = require('./oscillationMonitor');
process.env.NODE_ENV = 'test';


test('oscillation true ', (done) => {
  let value = 10001;
  let comparedValue = 10000;
  let percent = 0.1;
  let expected = false
  expect(oscillationMonitor.prototype.oscillationCheck(value,comparedValue,percent)).toStrictEqual(expected);
  done();
});
test('oscillation false ', (done) => {
  let value = 10020;
  let comparedValue = 10000;
  let percent = 0.1;
  let expected = true
  expect(oscillationMonitor.prototype.oscillationCheck(value,comparedValue,percent)).toStrictEqual(expected);
  done();
});